#!/usr/bin/env python
# encoding: utf-8
from numpy import *

import numpy as np
import scipy.constants as co

import chemise as ch


MUN_E = 1e24

MUN_OMINUS = 1.2e22

MUN_O2MINUS =  7.1e21 # pure o2: 5e21;  air: 7.1e21

MUN_O3MINUS = 7.6e21

MUN_NO3MINUS = 6.6e21

MUN_NOPLUS = 8.e21

# Temporary ion mobiliy: use a better value

MUN_O4PLUS = 5e21

MUN_DEFAULT = 5.4e21

MUN_N2PLUS = 4e21 # 100 Td

MUN_O2PLUS = 5e21 # 100 Td



NAIR = 2.5e25#co.value('Loschmidt constant (273.15 K, 101.325 kPa)')

quantum_energy = 0.29 * co.eV

mO2p = 31.9983
mO2m = 31.9993
mO2 = 31.9988
mN2 = 28.0134
mN2p = 28.0129
mO4p = 63.9971
mN2O2p = 60.0117
mN4p = 56.0263
mOm = 15.995 # not real
mOp = 15.99437
mN = 14.0067
mNO = 30.0061
mH2O = 18.0153
mH2O_O2p = 50.0135
mH3Op = 19.0227
mH3Op_H2O = mH3Op + mH2O
mH3Op_H2O2 = mH3Op_H2O + mH2O
mH3Op_H2O3 = mH3Op_H2O2 + mH2O
mO2m_H2O = mO2m + mH2O
mO2m_H2O2 = mO2m_H2O + mH2O
mO2m_H2O3 = mO2m_H2O2 + mH2O


class HumidAir(ch.ReactionSet):



    DEFAULT_REDUCED_MOBILITIES = {

        'e'   : MUN_E,

        #'O-'  : MUN_OMINUS,

        #'O2-' : MUN_O2MINUS,

        #'O3-' : MUN_O3MINUS,

        #'O4+' : MUN_O4PLUS,

        #'N4+' : MUN_DEFAULT,

        #'N2+' : MUN_N2PLUS,

        #'O2+' : MUN_O2PLUS,

        #'NO+' : MUN_DEFAULT,

        #'O+'  : MUN_DEFAULT,

        #'N2O2+' : MUN_DEFAULT,

        #'O2+.(H2O)': MUN_DEFAULT,

        #'H3O+': MUN_DEFAULT,

        #'H3O+.(H2O)': MUN_DEFAULT,

        #'H3O+.(H2O)2': MUN_DEFAULT,

        #'O2^-.(H2O)': MUN_DEFAULT,

        #'O2^-.(H2O)2': MUN_DEFAULT,

        #'O2^-.(H2O)3': MUN_DEFAULT,

        #'O^-.(H2O)': MUN_DEFAULT,

        #'OH^-.(H2O)': MUN_DEFAULT,

        #'OH^-.(H2O)2': MUN_DEFAULT,

        #'OH^-.(H2O)3': MUN_DEFAULT,

        #'OH-': MUN_DEFAULT,

        #'H-': MUN_DEFAULT,

        #'H2O+': MUN_DEFAULT

    }


    def __init__(self, water_content=0.0, folder ="chemise/swarm/streamer/streamer_dry/",vib=False, extend=False):

        super(HumidAir, self).__init__()


        self.water_content = water_content
        self.folder = folder
        self.vib = vib


        # These are ignored products
        self.fix({

            #'OH' : 0.0,

            #'H': 0.0,

            #'N2O': 0.0,

            #'O3': 0.0,

            #'O(3P)': 0.0,

            #'O(1S)': 0.0,

            #'O': 0.0,

            #'H2O': self.water_content*NAIR,

            #'N2(ap)': 0.0,

            #'N2(A)': 0.,

            #'N2': 0.79*NAIR,

            #'O2': 0.21*NAIR,

            #'M': NAIR,

            #'H2': 0.0,

            #'O(1D)': 0.,

            #'N(2D)': 0.

            })


        # Direct ionization R1-2
        self.add("e + O2 -> 2 * e + O2+",ionization(4.9e-9*co.centi**3,657),ref='daSilva2013/JGR')
        self.add("e + N2 -> 2 * e + N2+",ionization(8.1e-9*co.centi**3,925),ref='daSilva2013/JGR')


        # Stepwise ionization R3-4 (Revised)
        self.add("e + NO -> NO+ + 2*e",ionization(5e-9*co.centi**3,460),ref='daSilva2013/JGR')
        self.add("e + O -> O+ + 2*e",ionization(4e-9*co.centi**3,713),ref='daSilva2013/JGR')


        # Associative ionization reaction R5-7 (Revised)
        self.add("N + O -> NO+ + e",Thermaldisso(2.5e-15*co.centi**3, 32000.0),ref="daSilva2013/JGR") # Correct this in default_vib.py
        self.add("N2(A) + N2(ap) -> N4+ + e", ch.Constant(5.e-11*co.centi**3), ref="daSilva2013/JGR")
        self.add("N2(ap) + N2(ap) -> N4+ + e", ch.Constant(2.e-10*co.centi**3), ref="daSilva2013/JGR")


        # Dissociative attachment R8
        self.add("e + O2 -> O + O-", diss_attachment(6.7e-13*co.centi**3,0.8,1.05,5.3),ref="daSilva2013/JGR")


        # Three body attachment R9 (Maybe we should use da Silva's rate k(Te,T))
        self.add("e + O2 + O2 -> O2- + O2",att(1.4e-29*co.centi**6,700,1,600),ref="daSilva2013/JGR")
        self.add("e + O2 + N2 -> O2- + N2",att(1.07e-31*co.centi**6,1500,2,70),ref="daSilva2013/JGR")
        self.add("e + O2 + O -> O2- + O",ch.Constant(1e-31*co.centi**6),ref="daSilva2013/JGR")

        # Electron recombination R10-14 (Revised)
        self.add("e + O2+ -> O + O(1D)",ETemperaturePower(2e-7*co.centi**3, 1.0),ref='daSilva2013/JGR')
        self.add("NO+ + e -> O + N(2D)",ETemperaturePower(4.e-7 * co.centi**3, 1.5),ref="daSilva2013/JGR")
        self.add("e + O4+ -> O2 + O2 ",ETemperaturePower(1.4e-6*co.centi**3, 0.5),ref='daSilva2013/JGR') #This rate comes from Kossyi; Aleksandrov says 4.2e-6 (see Lui2017)
        self.add("e + N2O2+ -> N2 + O2",ETemperaturePower(1.3e-6*co.centi**3, 0.5),ref='daSilva2013/JGR')
        self.add("e + O2+ + M -> O2 + M",ETemperaturePower(6e-27*co.centi**6, 1.5),ref='daSilva2013/JGR')


        # Detachment R15-24 (Revised)
        self.add("N2 + O- -> e + N2O", detachment(1.16e-12*co.centi**3,43.5),ref="daSilva2013/JGR")
        self.add("O- + O -> O2 + e",ch.Constant(5.e-10*co.centi**3),ref="daSilva2013/JGR")
        self.add("O- + N2(A) -> O + N2 + e", ch.Constant(2.2e-9*co.centi**3),ref="daSilva2013")
        self.add("O- + O2(a) -> O3 + e", ch.Constant(3.e-10*co.centi**3),ref="daSilva2013")
        self.add("O- + NO -> NO2 + e", ch.Constant(2.6e-10*co.centi**3),ref="daSilva2013")
        self.add("O2- + O -> O3 + e",ch.Constant(1.5e-10*co.centi**3),ref="dasilva2013")
        self.add("O2- + N2(A) -> O2 + N2 + e", ch.Constant(2.1e-9*co.centi**3),ref="daSilva2013")
        self.add("O2- + O2(a) -> 2*O2 + e", ch.Constant(2.e-10*co.centi**3),ref="daSilva2013")
        self.add("O2 + O2- -> e + O2 + O2",detachment_T(2e-10*co.centi**3,6034,1509),ref="daSilva2013/JGR")
        self.add("O3- + O -> 2*O2 + e", ch.Constant(3.e-10*co.centi**3),ref='daSilva2013')


        # Negative ion conversion R80-R83 (Revised)
        self.add("O- + O2(a) -> O2- + O", ch.Constant(1.e-10*co.centi**3),ref="daSilva2013/JGR")
        self.add("O2- + O -> O2 + O-",ch.Constant(3.3e-10*co.centi**3),ref="dasilva2013")
        self.add("O3- + O -> O2- + O2",ch.Constant(3.2e-10*co.centi**3),ref='daSilva2013/JGR')
        self.add("O2 + O- + M -> O3- + M",ion_conversion(mO2,mOm,1.08e-30*co.centi**6,1.0,0.0),ref="daSilva2013/JGR")


        # Positive ion conversion R68-79 (Revised)

        self.add("O4+ + O2(a) -> O2+ + 2*O2",ch.Constant(1e-10*co.centi**3),ref="daSilva2013/JGR")

        self.add("O4+ + O -> O2+ + O3",ch.Constant(3e-10*co.centi**3),ref="daSilva2013/JGR")

        self.add("O2+ + 2*O2 -> O4+ + O2",ion_conversion(mO2,mO2p,2.4e-30*co.centi**6,3.2,0.0),ref="daSilva2013/JGR")

        self.add("N2O2+ + O2 -> O4+ + N2",ch.Constant(1e-9*co.centi**3),ref="daSilva2013/JGR")

        self.add("O2+ + 2*N2 -> N2O2+ + N2",ion_conversion(mN2,mO2p,0.9e-30*co.centi**6,2.0,0.0),ref="daSilva2013/JGR")

        self.add("O4+ + N2 -> N2O2+ + O2",ion_conversion(mN2,mO4p,4.61e-12*co.centi**3,2.5,2650.0),ref="daSilva2013/JGR") # Corregir en default_vib.py

        self.add("N2O2+ + N2 -> O2+ + 2*N2",ion_conversion(mN2,mN2O2p,1.1e-6*co.centi**3,5.3,2357.0),ref="daSilva2013/JGR")

        self.add("O4+ + O2 -> O2+ + 2*O2",ion_conversion(mO2,mO4p,3.3e-6*co.centi**3,4.0,5030.0),ref="daSilva2013/JGR")

        self.add("O2+ + N2 -> NO+ + NO",ch.Constant(1e-17*co.centi),ref="daSilva2013/JGR")

        self.add("O2+ + N -> NO+ + O",ch.Constant(1.2e-10*co.centi**3),ref="daSilva2013/JGR")

        self.add("O2+ + NO -> NO+ + O2",ch.Constant(4.4e-10*co.centi**3),ref="daSilva2013/JGR")

        self.add("O4+ + NO -> NO+ + 2*O2",ch.Constant(1e-10*co.centi**3),ref="daSilva2013/JGR")



        # Electron impact dissociation R30-32 (Revised)
        self.add("N2 + e -> N + N(2D) + e",ionization(5e-9*co.centi**3,646),ref="daSilva2013/JGR")

        self.add("O2 + e -> O + O + e",

                 ranalytic(-0.6537,1.,-152.7,-6.674),ref="Bolsig")

        self.add("O2 + e -> O + O(1D) + e",

                 ranalytic(-0.1318,1.,-170.2,-7.226),ref="Bolsig")


        ########################################################################
        # Electronic excitations, R25-29
        ########################################################################

        self.add("e + N2 -> e + N2(A)",

                 LogLogInterpolate0F(self.folder + "N2_Excitation10.dat", extend=extend))

        self.add("e + N2 -> e + N2(A)",

                 LogLogInterpolate0F(self.folder + "N2_Excitation11.dat", extend=extend))

        self.add("e + N2 -> e + N2(A)",

                 LogLogInterpolate0F(self.folder + "N2_Excitation12.dat", extend=extend))

        self.add("e + N2 -> e + N2(B)",

                 LogLogInterpolate0F(self.folder + "N2_Excitation12.dat", extend=extend))

        self.add("e + N2 -> e + N2(ap)",

                 LogLogInterpolate0F(self.folder + "N2_Excitation16.dat", extend=extend))

        self.add("e + N2 -> e + N2(C)",

                 LogLogInterpolate0F(self.folder + "N2_Excitation19.dat", extend=extend))

        self.add("e + O2 -> e + O2(a)",

                 LogLogInterpolate0F(self.folder + "O2_Excitation7.dat", extend=extend))


        # Radiative decay (1PN2, 2PN2) R33-34
        self.add("N2(C) -> N2(B)",

                 ch.Constant(2e7),ref="daSilva2013/JGR") # 2PN2

        self.add("N2(B) -> N2(A)",

                 ch.Constant(1.7e5),ref='daSilva2013/JGR') # 1PN2


        # Quenching (Revised) R35-57
        self.add("N2(A) + O2 -> N2 + O + O",

                ch.Constant(2.54e-12*co.centi**3), ref="daSilva2013/JGR")

        self.add("N2(A) + O2 -> N2 + O2(b)",

                ch.Constant(7.5e-13*co.centi**3), ref="daSilva2013/JGR")

        self.add("N2(A) + O -> N2 + O(1S)",

                ch.Constant(3e-11*co.centi**3), ref="daSilva2013/JGR")

        self.add("N2(A) + O -> NO + N(2D)",

                ch.Constant(7e-12*co.centi**3), ref="daSilva2013/JGR")

        self.add("N2(A) + N2(A) -> N2(B) + N2",

                ch.Constant(7.7e-11*co.centi**3), ref="daSilva2013/JGR")

        self.add("N2(A) + N2(A) -> N2(C) + N2",

                ch.Constant(1.6e-10*co.centi**3), ref="daSilva2013/JGR")


        self.add("N2(B) + O2 -> N2 + O + O",ch.Constant(3e-10*co.centi**3),ref='daSilva2013/JGR') #Include this reaction in default_vib.py

        self.add("N2(B) + N2 -> N2(A) + N2",

                 ch.Constant(1e-11*co.centi**3),ref="daSilva2013/JGR")


        self.add("N2(ap) + O2 -> N2 + O + O(1D)",

                ch.Constant(2.8e-11*co.centi**3), ref="daSilva2013/JGR")

        self.add("N2(ap) + N2 -> N2(B) + N2",

                ch.Constant(2e-13*co.centi**3), ref="daSilva2013/JGR")



        self.add("N2(C) + O2 -> N2 + O + O(1D)",

                 ch.Constant(2.5e-10*co.centi**3),ref="daSilva2013/JGR")

        self.add("N2(C) + N2 -> N2(B) + N2",

                 ch.Constant(1.e-11*co.centi**3),ref="daSilva2013/JGR")

        self.add("N2(C) + N2 -> N2(ap) + N2",

                 ch.Constant(1.e-11*co.centi**3),ref="daSilva2013/JGR")



        self.add("O(1D) + N2 -> O + N2", Aleksandrov_short(1.8e-11*co.centi**3,-107.0),ref="dasilva2013")

        self.add("O(1D) + O2 -> O + O2(b)", Aleksandrov_short(2.56e-11*co.centi**3,-67.0),ref="dasilva2013")

        self.add("O(1D) + O2 -> O + O2", Aleksandrov_short(0.64e-11*co.centi**3,-67.0),ref="dasilva2013")


        self.add("O(1S) + O -> O(1D) + O(1D)",Aleksandrov_short(5e-11*co.centi**3,301.0),ref="daSilva2013/JGR")

        self.add("O(1S) + O2 -> O(1D) + O2",Aleksandrov_short(1.3e-12*co.centi**3,850.0),ref="daSilva2013/JGR")


        self.add("N(2D) + O2 -> NO + O",TemperaturePower(1.5e-12 * co.centi**3, -0.5),ref="daSilva2013/JGR")

        self.add("N(2D) + O2 -> NO + O(1D)",TemperaturePower(6e-12*co.centi**3,-0.5),ref='daSilva2013/JGR')

        self.add("N(2D) + N2 -> N + N2",ch.Constant(6e-15*co.centi**3),ref="daSilva2013")


        self.add("O2(a) + O2 -> O2 + O2",TemperaturePower(2.2e-18*co.centi**3,-0.8),ref="daSilva2013/JGR")



        ########################################################################
        # High temperature chemistry
        ########################################################################

        # Thermal dissociation and recombination (Revised)
        X = ['N2', 'O2', 'NO']

        # R58
        for species in X:
            self.add("N2 + " + species + " -> 2*N + " + species ,AleksandrovZ(5.e-8*co.centi**3, 113200.0, 3354.0), ref="daSilva2013")


        Y = ['O', 'NO']

        for species in Y:
            self.add("N2 + " + species + " -> 2*N + " + species,AleksandrovZ(1.1e-7*co.centi**3, 113200.0, 3354.0), ref="daSilva2013")

        # R59
        self.add("O2 + O2 -> 2*O + O2",Aleksandrov(3.7e-8*co.centi**3, 59380.0, 2240.0), ref="daSilva2013/JGR")

        self.add("O2 + O -> 3*O",Aleksandrov(1.3e-7*co.centi**3, 59380.0, 2240.0), ref="daSilva2013/JGR")

        X = ['N', 'NO', 'N2']

        for species in X:
            self.add("O2 + " + species + " -> 2*O + " + species,Aleksandrov(9.3e-9*co.centi**3, 59380.0, 2240.0), ref="daSilva2013")

        ########### R60
        self.add("NO + N2 -> N + O + N2",Aleksandrov_short(8.7e-9*co.centi**3,76000.0),ref="daSilva2013/JGR")
        self.add("NO + O2 -> N + O + O2",Aleksandrov_short(8.7e-9*co.centi**3,76000.0),ref="daSilva2013/JGR")

        self.add("NO + O -> N + O + O",Aleksandrov_short(1.7e-7*co.centi**3,76000.0),ref="daSilva2013/JGR")
        self.add("NO + NO -> N + O + NO",Aleksandrov_short(1.7e-7*co.centi**3,76000.0),ref="daSilva2013/JGR")
        self.add("NO + N -> N + O + N",Aleksandrov_short(1.7e-7*co.centi**3,76000.0),ref="daSilva2013/JGR")

        # R61-63
        self.add("N + N + M -> N2 + M",Aleksandrov_short(8.27e-34*co.centi**6,-500.0),ref="daSilva2013/JGR")
        self.add("O + O + N2 -> O2 + N2",Aleksandrov_short(2.76e-34*co.centi**6,-720.0),ref="daSilva2013/JGR")
        self.add("O + O + N -> O2 + N",Aleksandrov_short(2.76e-34*co.centi**6,-720.0),ref="daSilva2013/JGR")
        self.add("O + O + NO -> O2 + NO",Aleksandrov_short(2.76e-34*co.centi**6,-720.0),ref="daSilva2013/JGR")
        self.add("O + O + O2 -> O2 + O2",Aleksandrov_T(2.45e-31*co.centi**6,-0.63),ref="daSilva2013/JGR")
        self.add("O + O + O -> O2 + O",Aleksandrov_T(8.8e-31*co.centi**6,-0.63),ref="daSilva2013/JGR")
        self.add("N + O + M -> NO + M",Aleksandrov_T(1.76e-31*co.centi**6,-0.5),ref="daSilva2013/JGR")



        # Exchange of chemical bonds R64-67(Revised)
        self.add("O + N2 -> N + NO",Aleksandrov_short(1.3e-10*co.centi**3,38000.0),ref="daSilva2013/JGR")
        self.add("N + O2 -> O + NO",Aleksandrov_exp_T(1e-14*co.centi**3, 3150.0),ref="daSilva2013/JGR")
        self.add("N + NO -> O + N2",Aleksandrov_T(1e-12*co.centi**3,0.5),ref="daSilva2013/JGR")
        self.add("O + NO -> N + O2",Aleksandrov_exp_T(2.5e-15*co.centi**3,19500.0),ref="daSilva2013/JGR")


        # Two-body ion-ion recombination R84-89 R90-101 (Revised)
        self.add('O- + O2+ -> O + O2',ITemperaturePower(2e-7*co.centi**3,0.5),ref='daSilva2013/JGR')
        self.add('O2- + O2+ -> O2 + O2',ITemperaturePower(2e-7*co.centi**3,0.5),ref='daSilva2013/JGR')
        self.add('O3- + O2+ -> O3 + O2',ITemperaturePower(2e-7*co.centi**3,0.5),ref='daSilva2013/JGR')
        self.add('O- + NO+ -> O + NO',ITemperaturePower(2e-7*co.centi**3,0.5),ref='daSilva2013/JGR')
        self.add('O2- + NO+ -> O2 + NO',ITemperaturePower(2e-7*co.centi**3,0.5),ref='daSilva2013/JGR')
        self.add('O3- + NO+ -> O3 + NO',ITemperaturePower(2e-7*co.centi**3,0.5),ref='daSilva2013/JGR')

        self.add('O- + O2+ -> 3*O',ch.Constant(1e-7*co.centi**3),ref='daSilva2013/JGR')
        self.add('O- + NO+ -> 2*O + N',ch.Constant(1e-7*co.centi**3),ref='daSilva2013/JGR')
        self.add('O- + O4+ -> O + 2*O2',ch.Constant(1e-7*co.centi**3),ref='daSilva2013/JGR')
        self.add('O- + N2O2+ -> O + O2 + N2',ch.Constant(1e-7*co.centi**3),ref='daSilva2013/JGR')
        self.add('O2- + O2+ -> O2 + 2*O',ch.Constant(1e-7*co.centi**3),ref='daSilva2013/JGR')
        self.add('O2- + NO+ -> O2 + O + N',ch.Constant(1e-7*co.centi**3),ref='daSilva2013/JGR')
        self.add('O2- + O4+ -> 3*O2',ch.Constant(1e-7*co.centi**3),ref='daSilva2013/JGR')
        self.add('O2- + N2O2+ -> 2*O2 + N2',ch.Constant(1e-7*co.centi**3),ref='daSilva2013/JGR')
        self.add('O3- + O2+ -> O3 + 2*O',ch.Constant(1e-7*co.centi**3),ref='daSilva2013/JGR')
        self.add('O3- + NO+ -> O3 + N + O',ch.Constant(1e-7*co.centi**3),ref='daSilva2013/JGR')
        self.add('O3- + O4+ -> O3 + 2*O2',ch.Constant(1e-7*co.centi**3),ref='daSilva2013/JGR')
        self.add('O3- + N2O2+ -> O3 + O2 + N2',ch.Constant(1e-7*co.centi**3),ref='daSilva2013/JGR')


        # Three-body ion-ion recombination R102-R106(Revised)
        self.add('O- + O2+ + O2 -> O + O2 + O2',ITemperaturePower(2e-25*co.centi**6,2.5),ref='daSilva2013/JGR')
        self.add('O2- + O2+ + O2 -> O2 + O2 + O2',ITemperaturePower(2e-25*co.centi**6,2.5),ref='daSilva2013/JGR')
        self.add('O- + NO+ + O2 -> O + NO + O2',ITemperaturePower(2e-25*co.centi**6,2.5),ref='daSilva2013/JGR')
        self.add('O2- + NO+ + O2 -> O2 + NO + O2',ITemperaturePower(2e-25*co.centi**6,2.5),ref='daSilva2013/JGR')
        self.add('O2- + O4+ + O2 -> 3*O2 + O2',ITemperaturePower(2e-25*co.centi**6,2.5),ref='daSilva2013/JGR')

        self.add('O- + O2+ + N2 -> O + O2 + N2',ITemperaturePower(2e-25*co.centi**6,2.5),ref='daSilva2013/JGR')
        self.add('O2- + O2+ + N2 -> O2 + O2 + N2',ITemperaturePower(2e-25*co.centi**6,2.5),ref='daSilva2013/JGR')
        self.add('O- + NO+ + N2 -> O + NO + N2',ITemperaturePower(2e-25*co.centi**6,2.5),ref='daSilva2013/JGR')
        self.add('O2- + NO+ + N2 -> O2 + NO + N2',ITemperaturePower(2e-25*co.centi**6,2.5),ref='daSilva2013/JGR')
        self.add('O2- + O4+ + N2 -> 3*O2 + N2',ITemperaturePower(2e-25*co.centi**6,2.5),ref='daSilva2013/JGR')


        self.initialize()  # Adding reactions



def cluster(n):

    if n == 0:

        return "H3O+"

    if n == 1:

        return "H3O+.(H2O)"

    else:

        return "H3O+.(H2O)%d" % n


class TemperaturePower(ch.Rate):

    def __init__(self, k0, power, T0=300):

        self.k0 = k0

        self.power = power

        self.T0 = T0



    def __call__(self, EN, T, Tv):

        return full_like(EN, self.k0 * (self.T0 / T)** self.power)



    def latex(self):

        return (r"$\num{%g} \times (\num{%g} / T)^{\num{%g}}$"

                % (self.k0, self.T0, self.power))

class ETemperaturePower(ch.Rate):

    def __init__(self, k0, power, T0=300):

        self.k0 = k0

        self.power = power

        self.T0 = T0



    def __call__(self, EN, T, Tv):

        Te = T + 3648.6*EN**0.46

        return full_like(EN, self.k0 * (self.T0 / Te)** self.power)



    def latex(self):

        return (r"$\num{%g} \times (\num{%g} / T_e)^{\num{%g}}$"

                % (self.k0, self.T0, self.power))

class PancheshnyiFitEN(ch.Rate):

    def __init__(self, k0, a, b):

        self.k0 = k0

        self.a = a

        self.b = b



    def __call__(self, EN, T, Tv):

        return self.k0 * exp(-(self.a / (self.b + EN))**2)



    def latex(self):

        self.k_0 = self.k0

        params = ["$%s = \\num{%g}$" % (s, getattr(self, s))

                  for s in ('k_0', 'a', 'b')]



        return ("$k_0e^{-\\left(\\frac{a}{b + E/n}\\right)^2}$ [%s]"

                % ", ".join(params))

class PancheshnyiFitEN2(ch.Rate):

    def __init__(self, k0, a):

        self.k0 = k0

        self.a = a



    def latex(self):

        self.k_0 = self.k0

        params = ["$%s = \\num{%g}$" % (s, getattr(self, s))

                  for s in ('k_0', 'a')]



        return ("$k_0e^{-\\left(\\frac{E/n}{a}\\right)^2}$ [%s]"

                % ", ".join(params))



    def __call__(self, EN, T, Tv):

        return self.k0 * exp(-(EN / self.a)**2)

class Gallimberti(ch.Rate):

    def __init__(self, kforward, n0, deltag, g):

        self.k0 = kforward * n0

        self.deltag = deltag

        self.g = g

    def latex(self):

        params = ["$%s = \\num{%g}$" % (s, v)

                  for s, v in (('k_0', self.k0),

                               ('\Delta G', self.deltag),

                               ('g', self.g))]



        return ("$k_0 e^{\\left(\\frac{\Delta G}{kT_i}\\right)}; T_i=T + \\frac{1}{g}\\frac{E}{n}$ [%s]"

                % ", ".join(params))


    def __call__(self, EN, T, Tv):

        Ti = T + EN / self.g

        return self.k0 * exp(self.deltag / (co.k * Ti))

class att(ch.Rate):
    def __init__(self, a, b, c, d):

        self.a = a
        self.b = b
        self.c = c
        self.d = d

    def __call__(self, EN, T, Tv):

        Te = T + 3648.6 * (EN)**0.46

        return self.a*np.exp(self.b*(1/T - 1/Te))/((Te/300.)**self.c*np.exp(self.d/T))

class ranalytic(ch.Rate):

    def __init__(self, a, b, c, d):

        self.a = a
        self.b = b
        self.c = c
        self.d = d

    def __call__(self, EN, T, Tv):
        F = np.exp(6500 * np.exp(- quantum_energy / (co.k * Tv)) / EN**2)
        logk = self.a*np.log10(EN)**self.b + self.c/EN + self.d*(1.<EN)
        return F*co.centi**3*10**logk

class excitation(ch.Rate):

    def __init__(self, a, b, c, d, f):

        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.f = f


    def __call__(self,EN,T, Tv):
        logk = (-self.a - self.b/EN)*(EN<self.f) + (-self.c - self.d/EN)*(self.f<=EN)
        return co.centi**3*10**logk

class Aleksandrov(ch.Rate):

    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def __call__(self,EN, T, Tv):

        return self.a*full_like(EN,(1.-np.exp(-self.c/T))*np.exp(-self.b/T))

class AleksandrovZ(ch.Rate):

    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def __call__(self,EN, T, Tv):
        fraction = (1.0 - np.exp(-quantum_energy/(co.k*Tv)))/(1 - np.exp(-quantum_energy/(co.k*T)))
        Tm = 1.0 / (1.0/Tv - 1.0/T)
        Z = fraction*np.exp(- (113260.0 - 3.0*T) / Tm)
        return self.a*full_like(EN,Z*(1.-np.exp(-self.c/T))*np.exp(-self.b/T))

class Aleksandrov_short(ch.Rate):

    def __init__(self,a,b):
        self.a = a
        self.b = b

    def __call__(self,EN, T, Tv):

        return self.a*full_like(EN,np.exp(-self.b/T))

class Aleksandrov_T(ch.Rate):

    def __init__(self,a,b):
        self.a = a
        self.b = b

    def __call__(self,EN, T, Tv):

        return self.a*full_like(EN,T**self.b)

class Aleksandrov_exp_T(ch.Rate):

    def __init__(self,a,b):
        self.a = a
        self.b = b

    def __call__(self,EN, T, Tv):

        return self.a*full_like(EN,T*np.exp(-self.b/T))

class TexpT(ch.Rate):

    def __init__(self,a,b,c):
        self.a = a
        self.b = b
        self.c = c

    def __call__(self,EN,T,Tv):

        return self.a * full_like(EN,(300./T)**self.b*np.exp(-self.c/T))

class Thermaldisso(ch.Rate):

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __call__(self, EN, T, Tv):

        return self.a*full_like(EN,T*np.exp(-self.b/T))

class ionization(ch.Rate):

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __call__(self, EN, T, Tv):
        F = np.exp(6500 * np.exp(- quantum_energy / (co.k * Tv)) / EN**2)

        return self.a*full_like(EN,F*np.exp(-self.b/EN))

class diss_attachment(ch.Rate):

    def __init__(self, a, b, c, d):
        self.a = a
        self.b = b
        self.c = c
        self.d = d

    def __call__(self, EN, T, Tv):
        F = np.exp(6500 * np.exp(- quantum_energy / (co.k * Tv)) / EN**2)

        return self.a*F*EN**self.b/np.exp(self.c*np.abs(self.d-np.log(EN))**3)

class detachment(ch.Rate):

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __call__(self, EN, T, Tv):

        return self.a*full_like(EN,EN**2/(self.b**2 + EN**2))

class detachment_T(ch.Rate):

    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def __call__(self, EN, T, Tv):

        Ti = T + 0.13*EN**2
        Teff = (mO2*Ti + mO2m*T) / (mO2 + mO2m)
        diff = 1/T - 1/Teff
        return self.a*np.exp(-self.b/Teff)*(1-np.exp(-self.b*diff))/(1-np.exp(-self.c*diff))

class ion_conversion(ch.Rate):

    def __init__(self,ma,mb,a,b,c):
        self.ma = ma
        self.mb = mb
        self.a = a
        self.b = b
        self.c = c

    def __call__(self, EN, T, Tv):

        Ti = T + 0.13*EN**2
        Teff = (self.mb*T + self.ma*Ti) / (self.ma + self.mb)
        return self.a*(300.0/Teff)**self.b*np.exp(-self.c/Teff)

class ITemperaturePower(ch.Rate):

    def __init__(self, k0, power, T0=300):

        self.k0 = k0

        self.power = power

        self.T0 = T0



    def __call__(self, EN, T, Tv):

        Ti = T + 0.13*EN**2

        return full_like(EN, self.k0 * (self.T0 / Ti)** self.power)



    def latex(self):

        return (r"$\num{%g} \times (\num{%g} / T_e)^{\num{%g}}$"

                % (self.k0, self.T0, self.power))

class LogLogInterpolate0F(ch.LogLogInterpolate0):
    def __call__(self, *args):
        """Uses args[0] to interpolate, then multiplies by args[2]."""
        k0 = super(LogLogInterpolate0F, self).__call__(*args)
        F = np.exp(6500 * np.exp(- quantum_energy / (co.k * args[2])) / args[0]**2)

        return F * k0

#-------------------------------------------------------------------------------

#rs = HumidAir()

def chemical(rs,q,en,tgas,Tv):
    #tgas = 300
    dn_dt = rs.fderivs(q,en,tgas,Tv)

    return dn_dt

def write_chemistry(rs,t,q,en,tgas,Tv):
    rs.qtp_append_multi(t, qtp_args.qtp_path, q, en, tgas, Tv)


class qtp_args(object):

    def __init__(self):
        self.qtp_path = qtp_path
