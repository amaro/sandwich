
import glob
import os
import numpy as np
from scipy import interpolate
import scipy.constants as co
quantum_energy = 0.29 * co.eV

def ranalytic(a,b,c,d,EN,Tv):
    F = np.exp(6500 * np.exp(- quantum_energy / (co.k * Tv)) / EN**2)
    logk = a*np.log10(EN)**b + c/EN + d*(1.<EN)
    return F*co.centi**3*10**logk

def stepwise(a,b,EN,Tv):
    F = np.exp(6500 * np.exp(- quantum_energy / (co.k * Tv)) / EN**2)
    return F*a*np.full_like(EN,np.exp(-b/EN))

class reading_function():

    def __init__(self):

        a = {}
        c = {}
        for i, name in enumerate(glob.glob("chemise/swarm/swarm_dry/energy_loss/k*.dat")):
            a[str(i)] = name


        k_N2_vib = 0.0
        k_O2_rot = 0.0
        k_N2_rot = 0.0
        k_N2_el = 0.0
        k_O2_el = 0.0
        k_O2_vib = 0.0
        k_O2_excitation = 0.0
        k_N2_excitation = 0.0
        k_O2_ion = 0.0
        k_N2_ion = 0.0

        b = sorted(a.values())
        for file in b:
            with open(file) as fp:
                lines = fp.readlines()

                for i, line in enumerate(lines):

                    if (('N2' in line) and ('Excitation vib' in line)):
                        data = np.loadtxt(file,skiprows=2)
                        data = data.T
                        E = data[0]
                        k_N2_vib += data[1]
                    if (('O2' in line) and ('rot' in line)):
                        data = np.loadtxt(file,skiprows=2)
                        data = data.T
                        E = data[0]
                        k_O2_rot += data[1]
                    if (('N2' in line) and ('rot' in line)):
                        data = np.loadtxt(file,skiprows=2)
                        data = data.T
                        E = data[0]
                        k_N2_rot += data[1]
                    if (('O2' in line) and (('Elastic' in line) or ('Effective' in line))):
                        data = np.loadtxt(file,skiprows=2)
                        data = data.T
                        E = data[0]
                        k_O2_el += data[1]
                    if (('N2' in line) and (('Elastic' in line) or ('Effective' in line))):
                        data = np.loadtxt(file,skiprows=2)
                        data = data.T
                        E = data[0]
                        k_N2_el += data[1]
                    if (('O2' in line) and ('vib' in line)):
                        data = np.loadtxt(file,skiprows=2)
                        data = data.T
                        E = data[0]
                        k_O2_vib += data[1]
                    if (('Excitation  ' in line) and ('O2' in line)):
                        data = np.loadtxt(file,skiprows=2)
                        data = data.T
                        E = data[0]
                        k_O2_excitation += data[1]
                    if (('Excitation  ' in line) and ('N2' in line)):
                        data = np.loadtxt(file,skiprows=2)
                        data = data.T
                        E = data[0]
                        k_N2_excitation += data[1]
                    if (('Ionization' in line) and ('O2' in line)):
                        data = np.loadtxt(file,skiprows=2)
                        data = data.T
                        E = data[0]
                        k_O2_ion += data[1]
                    if (('Ionization' in line) and ('N2' in line)):
                        data = np.loadtxt(file,skiprows=2)
                        data = data.T
                        E = data[0]
                        k_N2_ion += data[1]

        self.k_N2_ion = interpolate.interp1d(E,k_N2_ion)
        self.k_O2_ion = interpolate.interp1d(E,k_O2_ion)
        self.k_N2_excitation = interpolate.interp1d(E,k_N2_excitation)
        self.k_O2_excitation = interpolate.interp1d(E,k_O2_excitation)
        self.k_O2_vib = interpolate.interp1d(E,k_O2_vib)
        self.k_N2_vib = interpolate.interp1d(E,k_N2_vib)
        self.k_O2_rot = interpolate.interp1d(E,k_O2_rot)
        self.k_N2_rot = interpolate.interp1d(E,k_N2_rot)
        self.k_N2_el = interpolate.interp1d(E,k_N2_el)
        self.k_O2_el = interpolate.interp1d(E,k_O2_el)



    def energy_density_rate(self,n_O2,n_N2,n_e,n_NO,n_O,E,Tv):

        F = np.exp(6500 * np.exp(- quantum_energy / (co.k * Tv)) / E**2)

        Q_ion = (self.k_O2_ion(E) * n_O2 + self.k_N2_ion(E) * n_N2) * n_e
        Q_exc = (self.k_O2_excitation(E) * n_O2 + self.k_N2_excitation(E) * n_N2) * n_e

        Q_E = F*(Q_ion + Q_exc)
        Q_E += n_e*n_N2*ranalytic(0.9387,1.,-262.5,-10.38,E,Tv)*13.0 + n_e*n_O2*ranalytic(-0.6537,1.,-152.7,-6.674,E,Tv)*5.9 + \
                    n_e*n_O2*ranalytic(-0.1318,1.,-170.2,-7.226,E,Tv)*8.34 + n_e*n_NO*stepwise(5e-9*co.centi**3,460,E,Tv)*9.26 + \
                    n_e*n_O*stepwise(4e-9*co.centi**3,713,E,Tv)*13.62

        Q_T = 0.3 * Q_E

        Q_V = self.k_N2_vib(E) * n_N2 * n_e
        Q_V -=(F-1)*Q_E/F
        Q_L = ((self.k_O2_el(E) + self.k_O2_rot(E) + self.k_O2_vib(E)) * n_O2 + (self.k_N2_el(E) + self.k_N2_rot(E)) * n_N2) * n_e

        return Q_T*co.eV, Q_L*co.eV, Q_V*co.eV
