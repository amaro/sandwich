#!/usr/bin/env python
# encoding: utf-8
r"""
Compressible Euler flow in cylindrical symmetry
===============================================

Solve the Euler equations of compressible fluid dynamics in 2D r-z coordinates:

.. math::
    \rho_t + (\rho u)_x + (\rho v)_y & = - \rho v / r \\
    (\rho u)_t + (\rho u^2 + p)_x + (\rho uv)_y & = -\rho u v / r \\
    (\rho v)_t + (\rho uv)_x + (\rho v^2 + p)_y & = - \rho v^2 / r \\
    E_t + (u (E + p) )_x + (v (E + p))_y & = - (E + p) v / r.

Here :math:`\rho` is the density, (u,v) is the velocity, and E is the total energy.
The radial coordinate is denoted by r.
"""
import numpy as np
import scipy.constants as co
from scipy import interpolate
from scipy.optimize import fsolve
import time

import euler_drift # Riemann solver
import poisson_mg as poisson # Electromagnetic module
import default_dasilva as chemistry # Chemistry module
from clawpack import riemann

from mpi4py import MPI

import heating#_humid as heating
heating.reading_function()

# Quantum energy
quantum_energy = 0.29 * co.eV

# Average molecular weight of air
mair = 28.97 * co.gram / co.N_A

# Number density of air
nair = 2.5e25#co.value('Loschmidt constant (273.15 K, 101.325 kPa)')

# Mass density of air
rhoair = mair * nair

# Start settings class
import settings
settings = settings.settings()
settings()

settings.chem()

# Initiate air chemistry class
air = chemistry.HumidAir(water_content=settings.water_content,folder=settings.folder,vib=settings.vib)

nspecies = air.nspecies
dspecies = air.dspecies

# Sign of the electric charge
pos = [s for s in dspecies if '+' in s]
neg = [s for s in dspecies if '-' in s]

charged_pos = {}
for species in pos:
    charged_pos[species] = 1
charged_neg = {}
for species in neg:
    charged_neg[species] = -1
charged_neg['e'] = - 1

charged_species = dict(charged_neg,**charged_pos)
charge_sign = air.species_vector(charged_species)

# Space parameters
dimx = settings.mx
dimy = settings.my
az = settings.az
ar = settings.ar
bz = settings.bz
br = settings.br
dz = (bz - az)/dimx
dr = (br - ar)/dimy
outdir=settings.output

class density():

    def __init__(self,Nair_centre=nair,Nair_edge=nair,z=None,ze=None):
        self.Nair_centre = Nair_centre
        self.Nair_edge = Nair_edge
        self.ze = ze
        self.z = z

    def profile(self, H = 7.2e3):
        if settings.constant_density == False:
            if self.ze is not None:
                self.Nair_edge = nair*np.exp(-self.ze[...,None]/H)
            if self.z is not None:
                self.Nair_centre = nair*np.exp(-self.z[...,None]/H)

# Start density class
air_density = density()

# Labels for fluid equations
density = 0
x_momentum = 1
y_momentum = 2
trans_energy = 3
vib_energy = 4

euler_num_eqn = 5
num_eqn = euler_num_eqn + nspecies

# Mobility of species
mu = air.species_vector(air.DEFAULT_REDUCED_MOBILITIES)

# Useful labels
e = dspecies['e'] + euler_num_eqn
O2p = dspecies['O2+'] + euler_num_eqn
N2p = dspecies['N2+'] + euler_num_eqn
O2 = dspecies['O2'] + euler_num_eqn
N2 = dspecies['N2'] + euler_num_eqn
M = dspecies['M'] + euler_num_eqn
#H2O = dspecies['H2O'] + euler_num_eqn
NO = dspecies['NO'] + euler_num_eqn
O = dspecies['O'] + euler_num_eqn
N = dspecies['N'] + euler_num_eqn

# Useful constants
gamma = 1.4 # Ratio of specific heats
eta = 0.5 # Fraction of the dissipated energy that goes into gas heating
epsilon_0 = co.epsilon_0
charge = co.e

def qinit(state):

    import profile

    '''filename = 'claw0100.hdf'
    path = './_output/'

    var = 'q'
    if MPI.COMM_WORLD.Get_rank()==0:
        q, time = read(path,filename,var)
    else:
        q = None
        time = None

    state.t = MPI.COMM_WORLD.bcast(time,root=0)

    ranges = state.q_da.getRanges()
    if q is not None:
        state.q[:8] = split(q[:8],ranges)
        state.q[8:] = split(q[8:],ranges)
    else:
        state.q[:8] = split(q,ranges)
        state.q[8:] = split(q,ranges)'''

    grid = state.grid
    z, r = grid.p_centers

    temperature = 300.0
    pressure = co.k * nair * temperature

    state.q[density] = mair * nair
    state.q[x_momentum] = 0.0
    state.q[y_momentum] = 0.0

    # For nonzero velocity we also need the mechanical energy term
    state.q[trans_energy] = pressure / (gamma -  1.0)

    Tv = temperature
    state.q[vib_energy] = (0.79 * nair) * quantum_energy / (np.exp(quantum_energy / (co.k * Tv)) - 1)

    state.q[e:] = 0.
    sigma = 0.3e-3
    depletion_width = 3e-3
    depletion_depth = 0.5
    adepl = 1 - depletion_depth
    ne0_seed = 2e20
    z0 = bz
    #state.q[e] += 1e15
    state.q[e] += ne0_seed*np.exp(-r**2/sigma**2)#*(1-adepl*np.exp(-(z-bz/2)**2/depletion_width**2))

    state.q[O2p] = 0.21 * state.q[e] # N_O2+
    state.q[N2p] = 0.79 * state.q[e] # N_N2+

    state.q[O2] = 0.21 * nair
    state.q[N2] = 0.79 * nair
    state.q[M] = nair

    #state.q[euler_num_eqn + dspecies['H2O']] = settings.water_content * nair


def auxinit(state):
    """
    aux[0,i,j] = radial coordinate of cell centers for cylindrical source terms
    """
    y = state.grid.y.centers
    for j, r in enumerate(y):
        state.aux[0, :, j] = r

    z, r = state.grid.x.centers,state.grid.y.centers
    ze,re = state.grid.x.nodes, state.grid.y.nodes

    # Compute charge density and call Poisson equation solver
    dens = np.sum(charge_sign[...,None,None] * state.q[e:] * charge, axis=0)
    rhs = - dens/epsilon_0
    state.aux[-2] = dens

    # Potential (Domain decomposition)
    state.problem_data['IG'] = None
    state.problem_data['IGDM'] = None
    state.problem_data['IGDM1'] = None
    start = time.time()
    edges = True # To perform the calculation of the electric field at the edges
    Ez,Er,Eze,Ere = poisson.efield(-rhs, state, edges)
    end = time.time()
    print (end-start)

    # Background electric field
    Ebk = state.problem_data['Ebk']
    Ez += Ebk
    # Electric field at the cell edges
    Eze += Ebk # E_{i-1/2,j}

    if (z[0]<bz/2-dz<z[-1]):
        initial_guess = 2.741079865e6
        start = time.time()
        Ez0 = - fsolve(function,initial_guess,args=(r,state.q))
        end = time.time()
        print (end -start)
    else: Ez0 = None

    Ez0 = poisson.scattertoall(Ez0,MPI.COMM_WORLD)
    Ez0 = np.nanmin(Ez0)

    Ez += Ez0
    Eze  += Ez0

    Nair_centre = air_density.Nair_centre
    air_density.ze = ze
    air_density.profile()
    Nair_edge = air_density.Nair_edge

    E_interp = interp(z,r,1.e21*np.sqrt(Ez**2 + Er**2)/Nair_centre)

    E_xe = E_interp(ze,r)
    E_ye = E_interp(z,re)
    # Initialize aux vecs devoted to the electric field and drift velocities
    mu_e_xe = charge_sign[dspecies['e']]*settings.mu_e(E_xe)/Nair_edge
    mu_e_ye = charge_sign[dspecies['e']]*settings.mu_e(E_ye)/Nair_centre

    mobility_x = np.zeros((nspecies,mu_e_xe.shape[0],mu_e_xe.shape[1]))
    mobility_y = np.zeros((nspecies,mu_e_ye.shape[0],mu_e_ye.shape[1]))
    mobility_x[0] = mu_e_xe
    mobility_x[1:] = (mu[1:]*charge_sign[1:]/Nair_edge)[...,None,None]
    mobility_y[0] = mu_e_ye
    mobility_y[1:] = (mu[1:]*charge_sign[1:]/Nair_centre)[...,None,None]

    state.aux[1] = Ez
    state.aux[2] = Er
    # We assume that dspecies['e']=0
    state.aux[3 : nspecies + 3] = mobility_x[:,:-1] * Eze[:-1][None,...]
    state.aux[nspecies + 3 : 2*nspecies + 3] = mobility_y[:,:,:-1] * Ere[:,:-1][None,...]

    # We assume that dspecies['e']=0
    state.problem_data['vze'] = mobility_x[:,-1] * Eze[-1][None,...]
    state.problem_data['vre'] = mobility_y[:,:,-1] * Ere[:,-1][None,...]

    state.problem_data['dN_dt'] = None
    state.problem_data['derived'] = 0.

    # Dictionary to manage aux writing
    dict_aux = {}
    dict_aux['write'] = True
    dict_aux['num_aux_tbw'] = 4 + 4
    dict_aux['list'] = np.array((1,2,2*nspecies+3,2*nspecies+4,2*nspecies+5,2*nspecies + 6,-2,-1))#,5,6,7,8,9,10))
    state.problem_data['set_aux_written'] = dict_aux

    # qtp
    if state.grid.y.nodes[0]==0.:
        x = state.grid.x.centers
        x = x[::100]
        qtp_path = x.tolist()
        for i, point in enumerate(qtp_path):
            qtp_path[i] = './' + outdir + '/qtp/' + str(point)

        conditions_list = ["E/n (Td)", "T(K)", "Tv(K)"]
        air.qtp_init_many(qtp_path, conditions_list)
        chemistry.qtp_args.qtp_path = qtp_path

    # For photoionization
    state.problem_data['ph1'] = None
    state.problem_data['ph1DM'] = None
    state.problem_data['ph2'] = None
    state.problem_data['ph2DM'] = None
    state.problem_data['ph3'] = None
    state.problem_data['ph3DM'] = None


def b4step2(solver,state):

    # Arrays of coordinates
    z, r = state.grid.x.centers,state.grid.y.centers
    ze,re = state.grid.x.nodes, state.grid.y.nodes

    # Compute charge density and call Poisson equation solver
    dens = np.sum(charge_sign[...,None,None] * state.q[e:] * charge, axis=0)
    rhs = - dens/epsilon_0
    state.aux[-2] = dens

    # Potential computation (Domain decomposition)
    edges = True
    Ez,Er,Eze,Ere  = poisson.efield(-rhs, state, edges)

    # Background electric field
    Ebk = state.problem_data['Ebk']
    Ez += Ebk
    # Electric field at the cell edges
    Eze += Ebk # E_{i-1/2,j}, i= 0.., j = 0...

    if (z[0]<bz/2-dz<z[-1]):
        initial_guess = initial_guess = np.abs(state.aux[1,0,0])
        Ez0 = - fsolve(function,initial_guess,args=(r,state.q))
    else: Ez0 = None

    Ez0 = poisson.scattertoall(Ez0,MPI.COMM_WORLD)
    Ez0 = np.nanmin(Ez0)

    Ez += Ez0
    Eze  += Ez0

    Nair = state.q[density]/mair
    E_interp = interp(z,r,1.e21*np.sqrt(Ez**2 + Er**2)/Nair)

    E_xe = E_interp(ze,r)
    E_ye = E_interp(z,re)

    N_interp = interp(z,r,Nair)
    # Air number density at the cell edges
    N_ye = N_interp(z,re)
    N_xe =  N_interp(ze,r)

    # Initialize aux vecs devoted to the electric field and drift velocities
    mu_e_xe = charge_sign[dspecies['e']]*settings.mu_e(E_xe)/N_xe
    mu_e_ye = charge_sign[dspecies['e']]*settings.mu_e(E_ye)/N_ye

    # Mobilities at cell edges
    mobility_x = np.zeros((nspecies,mu_e_xe.shape[0],mu_e_xe.shape[1]))
    mobility_y = np.zeros((nspecies,mu_e_ye.shape[0],mu_e_ye.shape[1]))
    mobility_x[0] = mu_e_xe
    mobility_x[1:] = (mu[1:]*charge_sign[1:])[...,None,None]/N_xe
    mobility_y[0] = mu_e_ye
    mobility_y[1:] = (mu[1:]*charge_sign[1:])[...,None,None]/N_ye

    state.aux[1] = Ez
    state.aux[2] = Er
    # We assume that dspecies['e']=0
    state.aux[3 : nspecies + 3] = mobility_x[:,:-1] * Eze[:-1][None,...]
    state.aux[nspecies + 3 : 2*nspecies + 3] = mobility_y[:,:,:-1] * Ere[:,:-1][None,...]

    # We assume that dspecies['e']=0
    state.problem_data['vze'] = mobility_x[:,-1] * Eze[-1][None,...]
    state.problem_data['vre'] = mobility_y[:,:,-1] * Ere[:,-1][None,...]


def dq_cEuler_radial(solver, state, dt):
    """
    This is a Clawpack-style source term routine, which approximates
    the integral of the source terms over a step.
    """
    q = state.q
    aux = state.aux
    Ez = aux[1]
    Er = aux[2]

    dq = dq_comp(state, q, Ez, Er, dt/2.)

    qstar = np.zeros(q.shape)
    qstar = q + dq

    ################Second order ##################################
    # Compute charge density and call Poisson equation solver
    dens = np.sum(charge_sign[...,None,None] * qstar[e:] * charge, axis=0)
    rhs = - dens/epsilon_0
    # Electric field (DDM)
    edges = None
    Ez,Er  = poisson.efield(-rhs, state, edges)
    Ez += state.problem_data['Ebk']

    z, r = state.grid.x.centers,state.grid.y.centers
    ze,re = state.grid.x.nodes, state.grid.y.nodes

    if (z[0]<bz/2-dz<z[-1]):
        initial_guess = initial_guess = np.abs(aux[1,0,0])
        Ez0 = - fsolve(function,initial_guess,args=(r,qstar))
    else: Ez0 = None

    Ez0 = poisson.scattertoall(Ez0,MPI.COMM_WORLD)
    Ez0 = np.nanmin(Ez0)
    Ez += Ez0

    dq = dq_comp(state, qstar, Ez, Er, dt)

    # In clawpack-style solvers the source terms update q.
    q += dq


def dq_comp(state, q, Ez, Er, dt):

    rad = state.aux[0]

    E = np.array((Ez,Er))

    rho = q[density]
    u   = q[x_momentum]/rho
    v   = q[y_momentum]/rho
    press  = (gamma - 1.) * ( q[trans_energy] - 0.5 * rho * ( u**2 + v**2 ) )
    # Mobility at the cell centers
    Nair = rho / mair
    mobility = mu[1:][...,None,None]/Nair
    Ered = 1e21*np.sqrt(Ez**2 + Er**2)/Nair

    mobility_e = (settings.mu_e(Ered)/Nair)
    mobility = np.append(mobility_e[None,...],mobility,axis=0)

    vdriftx = mobility * charge_sign[...,None,None] * Ez
    vdrifty = mobility * charge_sign[...,None,None] * Er

    vel_u = u[None,...] + vdriftx
    vel_v = v[None,...] + vdrifty
    vel = np.array((vel_u,vel_v)) # Space component first and species component after

    # Current density
    #jze = charge * vel[0,0] * q[e] * charge_sign[0]
    jzi = np.sum(charge * vel[0,1:] * q[e + 1:] * charge_sign[1:][...,None,None], axis=0)
    #jre = charge * vel[1,0] * q[e] * charge_sign[0]
    jri = np.sum(charge * vel[1,1:] * q[e + 1:] * charge_sign[1:][...,None,None], axis = 0)
    #je = np.array((jze,jre))
    ji = np.array((jzi,jri))

    # Electronic and ionic power densities
    #w_e = np.sum(je * E, axis=0)
    w_i = np.sum(ji * E, axis=0)

    # Translational-rotational and vibrational temperatures
    tgas = press / (co.k * Nair)
    Tv = quantum_energy/(co.k * np.log(q[N2]*quantum_energy/q[vib_energy] + 1.0))

    # Vector encoding dn/dt for each species
    super_dn_dt = chemistry.chemical(air, q[e:], Ered, tgas, Tv) # dn/dt

    dq = np.zeros(q.shape)

    dq[density] = -dt/rad * q[y_momentum]
    dq[x_momentum] = -dt/rad * rho*u*v
    dq[y_momentum] = -dt/rad * rho*v*v

    Q_VT = func_Q_VT(tgas,Tv,q[N2],q[O2],q[NO],q[N],q[O])

    # Power density (degrees of freedom)
    Q_T,Q_L,Q_V = heating.reading_function().energy_density_rate(q[O2],q[N2],q[e],q[NO],q[O],Ered,Tv)
    #Q_T,Q_L,Q_V = heating.reading_function().energy_density_rate(q[O2],q[N2],q[e],q[euler_num_eqn + dspecies['H2O']],q[euler_num_eqn + dspecies['NO']],q[euler_num_eqn + dspecies['O']],Ered,Tv)

    # Dissociation, Da Silva and Pasko R58 and R61
    S58 = Z(tgas,Tv) * state.q[N2]*(1.1e-7*(q[O] + q[N]) + \
            5e-8*(q[NO] + q[O2] + q[N2])) * np.exp(-113260./tgas) * (1 - np.exp(-3354./tgas)) * co.centi**3
    S61 = 8.27e-34 * np.exp(500.0/tgas) * q[N]**2 * q[M] * co.centi**6
    S_D = S58 - S61
    epsilon_D = 9.76 * co.eV
    Q_D = epsilon_D * S_D

    Q_Teff = Q_T + w_i + Q_D + Q_L + Q_VT
    Q_veff = Q_V - 2 * Q_D - Q_VT

    state.aux[2*nspecies + 3] = Q_T
    state.aux[2*nspecies + 4] = Q_D
    state.aux[2*nspecies + 5] = Q_L
    state.aux[2*nspecies + 6] = Q_V

    dq[trans_energy] = (-dt/rad * v * (q[trans_energy] + press)
                 + dt * Q_Teff)

    dq[vib_energy] = -dt/rad * v * q[vib_energy] + dt * Q_veff

    qbcstar = get_qbcstar_from_qstar(state,q)
    dq[e:] = -dt/rad * (q[e:]*vel[1]) + super_dn_dt * dt + diff(state,qbcstar[e:],Ered) * dt


    if (state.problem_data['photo'] is True) and (0.<state.t):

        Sph = np.maximum(poisson.photo(super_dn_dt[dspecies['V']],state),0.)
        dq[e] += Sph * dt
        dq[O2p] += Sph * dt


    # Save dNe_dt
    state.problem_data['dN_dt'] = np.abs(super_dn_dt)


    # Write chemistry
    en = 1.e21*np.sqrt(Ez**2 + Er**2)/(state.q[density]/mair)
    if state.grid.y.nodes[0]==0.:
        chemistry.write_chemistry(air,state.t,state.q[e:,::100,0],en[::100,0],tgas[::100,0],Tv[::100,0])


    return dq


def diff(state,qbc,en):

    re = state.grid.y.nodes
    r = state.aux[0]

    z, r = state.grid.x.centers,state.grid.y.centers
    ze,re = state.grid.x.nodes, state.grid.y.nodes
    E_interp = interp(state.grid.x.centers,state.grid.y.centers,en)

    E_xe = E_interp(ze,r)
    E_ye = E_interp(z,re)

    Nair = state.q[density]/mair
    N_interp = interp(z,r,Nair)
    # Air number density at the cell edges
    N_ye = N_interp(z,re)
    N_xe =  N_interp(ze,r)

    De_x = settings.D_e(E_xe)/N_xe
    De_y = settings.D_e(E_ye)/N_ye

    D_x = np.zeros((nspecies,De_x.shape[0],De_x.shape[1]))
    D_y = np.zeros((nspecies,De_y.shape[0],De_y.shape[1]))
    D_x[0] = De_x
    D_x[1:] = De_x/100.
    D_y[0] = De_y
    D_y[1:] = De_y/100.

    # Built upon two ghost cells
    if state.grid.x.nodes[-1] == bz:
        qbc[:,-1,2:-2] = qbc[:,-3,2:-2]
        qbc[:,-2,2:-2] = qbc[:,-3,2:-2]

    if state.grid.x.nodes[0] == az:
        qbc[:,0,2:-2] = qbc[:,2,2:-2]
        qbc[:,1,2:-2] = qbc[:,2,2:-2]

    if state.grid.y.nodes[-1] == br:
        qbc[:,2:-2,-1] = qbc[:,2:-2,-3]
        qbc[:,2:-2,-2] = qbc[:,2:-2,-3]

    if state.grid.y.nodes[0] == ar:
        qbc[:,2:-2,0] = qbc[:,2:-2,2]
        qbc[:,2:-2,1] = qbc[:,2:-2,2]

    # This derivatives has been built upon two ghost cells
    dn_dr_r = re[None,None,1:] * D_y[:,:,1:] * (qbc[:,2:-2,3:-1] - qbc[:,2:-2,2:-2])/dr**2
    dn_dr_l = re[None,None,:-1] * D_y[:,:,:-1] * (qbc[:,2:-2,2:-2] - qbc[:,2:-2,1:-3])/dr**2
    d2n_dr2 = (dn_dr_r - dn_dr_l)/r

    dn_dz_r = D_x[:,1:] * (qbc[:,3:-1,2:-2] - qbc[:,2:-2,2:-2])/dz**2
    dn_dz_l = D_x[:,:-1] * (qbc[:,2:-2,2:-2] - qbc[:,1:-3,2:-2])/dz**2
    d2n_dz2 = dn_dz_r - dn_dz_l

    f = d2n_dr2 + d2n_dz2



    return f


def interp(x,y,z):
    return interpolate.RectBivariateSpline(x,y,z)


def build_comm(state):
    world_rank = MPI.COMM_WORLD.Get_rank()
    zf = state.grid.x.nodes
    if zf[-1] == bz :
        color = 55
        key = 0
    else:
        color = 77
        key = 0

    comm_bound = MPI.COMM_WORLD.Split(color,key)

    return comm_bound


def lower_radial_bc(state,dim,t,qbc,auxbc,num_ghost):

    # Mirror
    for i in range(num_ghost):
        qbc[:,:,i] = qbc[:,:,num_ghost + 1 -i]
        qbc[y_momentum,:,i] = - qbc[y_momentum,:,num_ghost + 1 -i]

def lower_radial_auxbc(state,dim,t,qbc,auxbc,num_ghost):

    # Mirror
    for i in range(num_ghost):
        auxbc[:,:,i] = auxbc[:,:,num_ghost]
        auxbc[2,:,i] = - auxbc[2,:,num_ghost +1 - i]
        auxbc[nspecies + 3: 2*nspecies + 3,:,i] = - auxbc[nspecies + 3: 2*nspecies + 3,:,num_ghost +1 - i]

def upper_auxbc(state,dim,t,qbc,auxbc,num_ghost):

    # Extrapolation
    for i in range(num_ghost):
        auxbc[:,-num_ghost + i] = auxbc[:,-num_ghost - 1]
        auxbc[3:nspecies + 3,-num_ghost + i,num_ghost:-num_ghost] = state.problem_data['vze']
        auxbc[:,:,-num_ghost + i] = auxbc[:,:,-num_ghost - 1]
        auxbc[nspecies + 3: 2*nspecies + 3,num_ghost:-num_ghost,-num_ghost + i] = state.problem_data['vre']


def set_time_step(solver,solution):

    state = solution.state

    cfl = solver.cfl.get_cached_max()
    dt_clawpack = min(solver.dt_max,solver.dt * solver.cfl_desired / cfl)

    E = np.sqrt(state.aux[1]**2 + state.aux[2]**2)
    Nair = state.q[density]/mair
    sigma = charge * (settings.mu_e(1.e21*E/Nair)/Nair) * state.q[e]
    t_maxwell = co.epsilon_0/sigma
    t_maxwell = t_maxwell[np.isfinite(t_maxwell)]
    t_maxwell = t_maxwell[np.nonzero(t_maxwell)]
    t_maxwell = np.nanmin(t_maxwell)

    dN_dt = state.problem_data['dN_dt']


    if dN_dt is None: t_chemical = solver.dt_max

    else:
        t_chemical = np.nanmin(np.maximum(np.abs(state.q[e:]),1.e6)/np.maximum(np.abs(dN_dt),5e16))


    mintime = min(t_chemical,t_maxwell)
    mintimes = poisson.scattertoall(mintime,MPI.COMM_WORLD)
    mintime = np.amin(mintimes)

    solver.dt = min(dt_clawpack,solver.dt_max,mintime)*0.75

    # Computing min. chemical and Maxwell time for the output file
    t_chemical = poisson.scattertoall(t_chemical,MPI.COMM_WORLD)
    t_maxwell = poisson.scattertoall(t_maxwell,MPI.COMM_WORLD)
    t_chemical = np.amin(t_chemical)
    t_maxwell = np.amin(t_maxwell)


    if MPI.COMM_WORLD.Get_rank()==0:
        filename = './' + outdir + '/dt.dat'
        data = (state.t*1e9,t_chemical,t_maxwell,dt_clawpack,solver.dt,cfl)
        with open(filename,"a") as f:
            f.write( str(data)[1:-1] + "\n")


def read(path, filename,var):

    import h5py

    file = h5py.File(path + filename)
    group = file['patch1']
    q = np.array(group[var])
    time = group.attrs['t']

    return q, time


def split(b,ranges):

    a = poisson.tozero(ranges,MPI.COMM_WORLD)
    if MPI.COMM_WORLD.Get_rank()==0:
        a = np.array(a,dtype='i').reshape(MPI.COMM_WORLD.Get_size(),4)
        c = [b[:,xs:xe,ys:ye] for (xs,xe,ys,ye) in a]
    else: c = None

    q = MPI.COMM_WORLD.scatter(c,root=0)


    return q


def get_qbcstar_from_qstar(state,q):
    num_ghost = 2
    shape = [n + 2*num_ghost for n in state.grid.num_cells]

    da = state.q_da
    qglobal = da.createGlobalVector()
    qlocal = da.createLocalVector()
    qglobal_array = da.getVecArray(qglobal)

    qglobal_array[:,:] = np.moveaxis(q,0,2)
    da.globalToLocal(qglobal,qlocal)
    shape.insert(0,state.num_eqn)

    return qlocal.getArray().reshape(shape,order='F')


def function(x,r,q):
    x = np.abs(x)

    middle = int(q.shape[1]/2)
    Nair = q[density,middle]/mair
    cond_e = q[e,middle]  * charge * settings.mu_e(1e21*x/Nair) / Nair

    conduc = np.zeros((nspecies,q.shape[2]))
    conduc[0] = cond_e
    conduc[1:] = q[e + 1:,middle] * charge * mu[1:].reshape(nspecies-1,1)/Nair

    integrand = np.sum(conduc,axis=0) * r

    integral = 2. * np.pi * np.sum(integrand) * dr

    #integral = poisson.scattertoall(integral,MPI.COMM_WORLD)
    f = x * integral - settings.intensity

    return f


def Z(T,Tv):
    fraction = (1.0 - np.exp(-quantum_energy/(co.k*Tv)))/(1 - np.exp(-quantum_energy/(co.k*T)))
    Tm = 1.0 / (1.0/Tv - 1.0/T)

    return fraction*np.exp(- (113260.0 - 3.0*T) / Tm)


def func_Q_VT(T,Tv,n_N2,n_O2,n_NO,n_N,n_O):


    k_molecule = 6.4e-12*co.centi**3*np.exp(-137.0/T**(1/3))
    delta_N2 = 6.8/np.sqrt(T)
    delta_O2 = 7.0/np.sqrt(T)
    delta_NO = 6.9/np.sqrt(T)
    k_atom = 2.3e-13*co.centi**3*np.exp(-1280.0/T) + 2.7e-11*co.centi**3*np.exp(-10840.0/T)
    delta_N = 5.6/np.sqrt(T)
    delta_O = 5.8/np.sqrt(T)

    delta_j = np.array((delta_N2,delta_O2,delta_NO,delta_N,delta_O))
    n_j = np.array((n_N2,n_O2,n_NO,n_N,n_O))
    k_j = np.array((k_molecule,k_molecule,k_molecule,k_atom,k_atom))


    Losev = (1.0 - np.exp(-quantum_energy/(co.k*Tv[None,...])))/(1.0 - np.exp(-quantum_energy/(co.k*Tv[None,...]) + delta_j))
    Losev *= Losev

    inv_tau_VT = (1.0 - np.exp(-quantum_energy/(co.k*T)))*np.sum(k_j*n_j*Losev,axis=0)

    tau_VT = 1.0/inv_tau_VT

    Q_VT = (vibrational_energy(n_N2,Tv) - vibrational_energy(n_N2,T))/tau_VT

    return Q_VT

def vibrational_energy(n_N2,T):

    return n_N2 * quantum_energy / (np.exp(quantum_energy / (co.k * T)) - 1)



def setup(use_petsc=False, solver_type='classic', outdir=outdir,
          kernel_language='Fortran', disable_output=False, mx=dimx, my=dimy, tfinal=1000e-9,
          num_output_times = 1000, htmlplot=False, tfluct_solver=False):

    if use_petsc:
        import clawpack.petclaw as pyclaw
    else:
        from clawpack import pyclaw

    # Solver settings
    solver = pyclaw.ClawSolver2D(euler_drift)
    solver.step_source = dq_cEuler_radial
    solver.source_split = 1
    solver.limiters = 4
    solver.cfl_max = 5.e-1
    solver.cfl_desired = 1.e-1
    solver.before_step = b4step2
    solver.dimensional_split = True
    solver.order = 2
    solver.dt_variable = True
    solver.dt_initial = 1e-13
    solver.get_dt_new = set_time_step
    solver.dt_max = 1e-10
    solver.verbosity = 0

    solver.user_aux_bc_lower = lower_radial_auxbc
    solver.user_bc_lower = lower_radial_bc
    solver.user_aux_bc_upper = upper_auxbc

    solver.bc_lower[0]=pyclaw.BC.extrap
    solver.bc_upper[0]=pyclaw.BC.extrap
    solver.bc_lower[1]=pyclaw.BC.custom
    solver.bc_upper[1]=pyclaw.BC.extrap
    #Aux variable in ghost cells doesn't matter
    solver.aux_bc_lower[0]=pyclaw.BC.extrap
    solver.aux_bc_upper[0]=pyclaw.BC.custom
    solver.aux_bc_lower[1]= pyclaw.BC.custom
    solver.aux_bc_upper[1]=pyclaw.BC.custom

    claw = pyclaw.Controller()
    claw.solver = solver

    # restart options
    restart_from_frame = None

    if restart_from_frame is None:
        x = pyclaw.Dimension(az, bz, mx, name='x')
        y = pyclaw.Dimension(ar, br, my, name='y')
        domain = pyclaw.Domain([x, y])

        # I don't understand why this was not needed in the example code. AL.
        solver.num_waves = num_eqn
        solver.num_eqn = num_eqn
        solver.max_steps = 100000
        num_aux = nspecies*2 + 3 + 1 + 1 + 4

        state = pyclaw.State(domain, num_eqn, num_aux)
        if MPI.COMM_WORLD.Get_rank()==0:
            filename = './' + outdir + '/dt.dat'
            data = "Time " + " dt_chemical " + " dt_maxwell " + " dt_clawpack " + " dt " + " cfl " + " Intensity "
            with open(filename,"a") as f:
                f.write( data + "\n")
        state.problem_data['gamma'] = gamma
        settings.get_args(state,air)
        settings.mu_e = settings.interp_function(settings.mobility_data)
        settings.D_e = settings.interp_function(settings.diffusion_data)
        settings.nu_E = settings.interp_function(settings.electronic_power)
        settings.nu_L = settings.interp_function(settings.elastic_power)
        settings.nu_v = settings.interp_function(settings.vib_power)
        qinit(state)
        auxinit(state)
        claw.solution = pyclaw.Solution(state,domain)
        claw.num_output_times = num_output_times
    else:
        solver.num_waves = num_eqn
        solver.num_eqn = num_eqn
        solver.max_steps = 100000
        claw.solution = pyclaw.Solution(restart_from_frame,file_format='hdf5',read_aux=False)
        claw.solution.state.problem_data['gamma'] = gamma
        auxinit(claw.solution.state)
        claw.start_frame = restart_from_frame
        claw.num_output_times = num_output_times - restart_from_frame

    claw.keep_copy = False
    if disable_output:
        claw.output_format = None
    else: claw.output_format = 'hdf5'
    #claw.output_options['chunks'] = True
    claw.tfinal = tfinal
    claw.outdir = outdir
    claw.write_aux_init = True
    claw.write_aux_always = True


    return claw



if __name__=="__main__":
    from clawpack.pyclaw.util import run_app_from_main
    output = run_app_from_main(setup)
