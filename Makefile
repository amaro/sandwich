
all:
	make euler_drift.so

euler_drift.so: rpn2_euler_drift.f90
	f2py -m euler_drift -c rpn2_euler_drift.f90 --f90flags='-fcheck=all'


